/* Open */
function openNav(arg) {

         switch (arg) {
                 case 1:
                         document.getElementById("overlay-about").style.height = "100%";
                         break;
                 case 2:
                         document.getElementById("overlay-contact").style.height = "100%";
                         break;
                 case 3:
                         document.getElementById("overlay-impressum").style.height = "100%";
                         break;
                 case 4:
                         document.getElementById("overlay-exit").style.height = "100%";
                         break;
                 case 5:
                         document.getElementById("overlay-close").style.height = "100%";
                         break;
                 case 6:
                         document.getElementById("overlay-win").style.height = "100%";
                         break;
                 case 7:
                         document.getElementById("overlay-fail").style.height = "100%";
                         break;
         }
}

/* Close */
function closeNav(arg) {

         switch (arg) {
                 case 1:
                         document.getElementById("overlay-about").style.height = "0%";
                         break;
                 case 2:
                         document.getElementById("overlay-contact").style.height = "0%";
                         break;
                 case 3:
                         document.getElementById("overlay-impressum").style.height = "0%";
                         break;
                 case 4:
                         document.getElementById("overlay-exit").style.height = "0%";
                         break;
                 case 5:
                         document.getElementById("overlay-close").style.height = "0%";
                         break;
                 case 6:
                         document.getElementById("overlay-win").style.height = "0%";
                         break;
                 case 7:
                         document.getElementById("overlay-fail").style.height = "0%";
                         break;
         }
}