var p1_selectedGod = 0;
var p2_selectedGod = 0;
var p2isBot = false;
/*
$("div#god2")
  .mouseenter(function() {
    $("div#god2").find( "p" ).text( "mouse enter x ");
  })
  .mouseleave(function() {
    $("div#god2").find( "p" ).text( "mouse leave" );
  });
*/

$("#p1-god1").click( function() {
   $("div#p1-ChosenGod1").show();
   $("div#p1-ChosenGod2").hide();
   $("div#p1-ChosenGod3").hide();
   $("div#p1-ChosenGod4").hide();
   p1_selectedGod = "Ares";
});

$("#p1-god2").click( function() {
   $("div#p1-ChosenGod1").hide();
   $("div#p1-ChosenGod2").show();
   $("div#p1-ChosenGod3").hide();
   $("div#p1-ChosenGod4").hide();
   p1_selectedGod = "Loki";
});

$("#p1-god3").click( function() {
   $("div#p1-ChosenGod1").hide();
   $("div#p1-ChosenGod2").hide();
   $("div#p1-ChosenGod3").show();
   $("div#p1-ChosenGod4").hide();
   p1_selectedGod = "XipeTotec";
});

$("#p1-godrandom").click( function() {
   $("div#p1-ChosenGod1").hide();
   $("div#p1-ChosenGod2").hide();
   $("div#p1-ChosenGod3").hide();
   $("div#p1-ChosenGod4").show();
   p1_selectedGod = 0;
});

$("#p2-god1").click( function() {
   $("div#p2-ChosenGod1").show();
   $("div#p2-ChosenGod2").hide();
   $("div#p2-ChosenGod3").hide();
   $("div#p2-ChosenGod4").hide();
   p2_selectedGod = "Ares";
});

$("#p2-god2").click( function() {
   $("div#p2-ChosenGod1").hide();
   $("div#p2-ChosenGod2").show();
   $("div#p2-ChosenGod3").hide();
   $("div#p2-ChosenGod4").hide();
   p2_selectedGod = "Loki";
});

$("#p2-god3").click( function() {
   $("div#p2-ChosenGod1").hide();
   $("div#p2-ChosenGod2").hide();
   $("div#p2-ChosenGod3").show();
   $("div#p2-ChosenGod4").hide();
   p2_selectedGod = "XipeTotec";
});

$("#p2-godrandom").click( function() {
   $("div#p2-ChosenGod1").hide();
   $("div#p2-ChosenGod2").hide();
   $("div#p2-ChosenGod3").hide();
   $("div#p2-ChosenGod4").show();
   p2_selectedGod = 0;
});

// TOGGLE AI

$("#p2-toggleAI").click( function() {
   p2isBot = !p2isBot;
   if (p2isBot) {
      $(this).attr("src","img/ki_button_active.png");
      $(this).attr("title","Spieler 2 durch KI gesteuert");
   }
   else {
      $(this).attr("src","img/ki_button.png");
      $(this).attr("title","Menschlicher Spieler 2");
   }
});