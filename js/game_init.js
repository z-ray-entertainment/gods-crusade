function gotoCharSelect() {
    window.location.href = "charakterwahl.html";
}

function startGame() {
    //Check if someone has chosen random God
    if (p1_selectedGod == 0) {
         p1_selectedGod = randomGod(p1_selectedGod);
    }
    if (p2_selectedGod == 0) {
         p2_selectedGod = randomGod(p2_selectedGod);
    }

    //GET PLAYER NAMES
    var p1_name = document.getElementById('player1_name').value;
    p1_name = p1_name.replace(/<[^>]*>/g, "");
    var p2_name = document.getElementById('player2_name').value;
    p2_name = p2_name.replace(/<[^>]*>/g, "");
    //FILL WHEN PLAYER NAME EMPTY
    if (p1_name == "") { p1_name = fillEmptyPlayerName(1) };
    if (p2_name == "") { p2_name = fillEmptyPlayerName(2) };
    // alert(p1_name);
    //CHECK IF PLAYER 2 IS BOT
    //p2isBot = false;
    window.location.href = "game.html?started=true&p1_name=" + p1_name + "&p1_god=" + p1_selectedGod + "&p2_name=" + p2_name + "&p2_god=" + p2_selectedGod + "&isPlayer2KI=" + p2isBot;
}

function randomGod(randomGod) {
      randomGod = Math.random();
      randomGod *= 10;
      // alert(randomGod);
      if (randomGod <= 3.3) {
         return "Ares";
      }
      else if (randomGod >= 6.6) {
         return "XipeTotec";
      }
      else {
         return "Loki";
      }
}

function fillEmptyPlayerName(player) {
    if (player == 1) {
         return p1_selectedGod;
    }
    else {
         return p2_selectedGod;
    }
/*
    switch(fill_with_name_of_selected_god) {
        case 1:
           return "Ares";
        break;
        case 2:
           return "Loki";
        break;
        case 3:
           return "XipeTotec";
        break;
        default:
           return "";
    }
*/
}