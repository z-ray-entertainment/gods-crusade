function tabClose() {
      if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
        alert("Feature aufgrund von Sicherheitseinstellungen des Browsers gesperrt.\n" +
        "Gehe stattdessen zurueck zu Index.");
        window.location.href = "index.html";
      }
      else {
        var win = window.open("about:blank", "_self");
        win.close();
      }
}