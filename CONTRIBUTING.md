# Branching
As you may have noticed there are 3 branches available for the following
reasons:

## Master
The master brachn is the currentl live branch which is running on:
https://z-ray.de/godscrusade/
This always should be a stable and functoning branch.

##Stage
Is a pre release like branche which will also be deployed on the Z-Ray Homepage
for users who want to play the game with the latest features but may expect
some minor issues.
The Stage brachn is only merged with the master branch if it is well tested and
funcioning.

##Test
The test branch is meant to be the first branch where any developments from
any developer will be merged to gether and tested with each other.
The test brnach is only merged into the Stage branch if when it is found to be
stable

##Dev
Anybody is allowed to create as much development brnaches as needed, we
recomend one dev branch for one deveoper.
Ofcourse you're allowed to create different branches for different issues you're
working on if you find this helpfull.
But make sure not to commit to much branches you can leav you development
branches locally.

#Issues
Anybody is allowed to create issues for any purpose.

##Labeing issues
You're also allowed to add any lables except of "Aprroved" or "Assigned".
After an issue was commited it wil be revisited by a memeber of the head
development team and will be approved or rejected or will get additional issues.

##Working on issues
You're only allowed to work on an issue as long as it is asigned to you and labled
as approved

Thank you for contributing to this project.